# ThermalImage_processing

Scripts to process thermal images taken from handheld thermal cameras. 
1. `rawTempExtraction.R`  - reads the original JPEG file and output RAW temperature as  .txt file
2. `batchImgThermalFilter5.m` - for thresholding canopy pixels in ground thermal images



