%batchThermalFilter5 ::: histogram gradient percentage of Intensity
%  modified from batchThermalFilter3
% HIST. GRADIENT FOR THRESHOLDING

% created by ASA Salgadoe,  18/12/2018 [NEW_Best]
% Method for batch processing thermal images
% Estimating upper boundary and Lower boundary for TIR image filter points

%*****************************
% HItogram gradient
%USing the percentage of intensity over normal histogram
% getting the percnt. diff of intensity 

% BEfore run;
% set lower and upper boundary thresh limits
% Remember to CHANGE FOR LOOP SEq. 'NexT'
% To check RMSE need to create 'refftemp' table
% change the RMSE comparing table to reffTempShade or reffTempSun
% at final step cal. RMSE if changes done to # col, modifie

%*****************************

disp('start');
startT=fix(clock);
imagePath=('C:\Users\asalgado\UNE_Cloud_sync\PhD work\Pilot Test 3_Bundy\Analysis\Anlytical_original\img_Analysis(noReff)\TestSet\sun\');
srcFiles=dir(strcat(imagePath,'*.jpg'));

xlsx='C:\Users\asalgado\UNE_Cloud_sync\PhD work\Pilot Test 3_Bundy\Analysis\Anlytical_original\xlsx_RawValues\Daz\xlsx\';

results={'id','img','Ciba','Mode'};
for pin1=1:length(srcFiles)


imgName=srcFiles(pin1).name;
        
 
%Get the tree number from image
 imgNum= extractBefore(imgName,'_[');
% treeNum=extractBetween(imgName,'IR_','.jpg');

id=extractBetween(imgName,']_','.jpg');
% id=extractBetween(imgName,'^','.jpg');
ciba=extractBetween(imgName,'_[',']_');
% ciba=extractBetween(imgName,'_[','_');
id=char(id);
ciba=char(ciba);

% treeNum2=char(strcat('out_IR_',imgNum));
treeNum2=char(strcat('out_IR_',imgNum));
% treeNum2=imgNum;
% treeNum2=char(strcat(imgNum));%convertinto char
% treeNum2=char(strcat(imgNum));%convertinto char
xlsxFile=strcat(xlsx,treeNum2,'.xlsx');
% xlsxFile=strcat(xlsx,imgName,'.xlsx');
matrix=xlsread(xlsxFile);

xlsxFile=0;
imgNum=str2double(imgNum);%converting into double

 
%  Histogram Analysi>>>> Creating histogram 
% this will return the lower tail break point
% that means excluding any sudden rises but that does not mean removed sky
% area, becos sky area also can have smooth lower without rising
[modeLoc,modeTemp,mode,lowerBreak,pointer1,pointer2,pointer3,histArray,arrayDeri]=histogramValues(matrix,1.0);
total=numel(matrix);


% bins value is the end value of that bin start value is the previous
% eg: bin 20,21,22 (that means 20 to 21 details are in bin 21)
% likewise 21 to 22 details are in the bin 22. 

% Percentage of intensity
for a=1:length(histArray(:,1))
histArray(a,3)=(histArray(a,2)/total)*100;

end

%Subsetting the arrays>>>>>>>>>>>>
histArray_sub1=histArray(pointer3:modeLoc,1:2); %the lower part before mode
histArray_sub2=histArray(modeLoc+1:pointer1,1:2);% higher part from the mode



%>>>>>>>>>>>>>>>>>>>>>>>>Re-scaling  Y axis of histArray_sub (Intensity Percntage) 

%Standardising Y axis as a percentage of total
%0-100 scale
% otherwise the Y axis will be varying per file. Now any value will fall
% btween 0 to 100

for a=1:length(histArray_sub1(:,1))
histArray_sub1(a,3)=(histArray_sub1(a,2)/total)*100;

end
for a=1:length(histArray_sub2(:,1))
histArray_sub2(a,3)=(histArray_sub2(a,2)/total)*100;
end



% >>>>>>>>>>>>>>>>>>>>>>>>>>Calculating Intensity percntage Diff.
% columns order in histArray_sub;
% 1.bin,2.Pxlcount,3.Re-scaleY(intensity %),4.PositionY,5.Posi.X,
% 6.IntenctiyPernctDiff
% the diff is stored in the bin towrds the slop from Mode

for i=length(histArray_sub1(:,1))-1:-1:1
histArray_sub1(i,6)=histArray_sub1(i+1,3)-histArray_sub1(i,3);
% histArray_sub1(i,6)=atand((histArray_sub1(i+1,3)-histArray_sub1(i,3))/1);

end

for i=2:length(histArray_sub2(:,1))
histArray_sub2(i,6)=histArray_sub2(i-1,3)-histArray_sub2(i,3);
% histArray_sub2(i,6)=atand((histArray_sub2(i,3)-histArray_sub2(i+1,3))/1);
end


% >>>>>>>>>>>>>>>>>>>>>>>>>>> resultsRegistry

results{pin1+1,1}=id;
results{pin1+1,2}=imgName;
results{pin1+1,3}=ciba;
results{pin1+1,4}=modeTemp;
%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Estimating the Boundaries
% columns order in histArray_sub;
% 1.bin,2.Pxlcount,3.Re-scaleY(intensity %),4.PositionY,5.Posi.X,6.TanInvAngle


% **Should start serching the RIC thresh from top towards tails
% becos otherwise it will start to scan from sky pixels
% from canopy mode where it drops to certain value
% but sometimes near mode the curve tends to have less differance
% to avoid searchin threshold near the mode but at tail ends must start 
% searching from 2nd point after the mode

% % % % % % % % % % % % % % % % % % % Check
for NextT=1%looping number of thresholds
% 1:51
% % % % % % % % % % % % % % % % % % %     
lowerBndry=0;
upperBndry=0;


%Lower

% lower gradient Prct is lower
% % % % % % % % % % % % % % % % % % Check
% shade 1.1
% sun 0.5
% array of range/s of threshold/s
gradientLowerThreshould=0.5;
% 0.0:0.1:5.0;
% % % % % % % % % % % % % % % % % % 
% 0.03:0.08:0.2;
% 0.03:0.1:1.0
% 0.00:0.1:5.0
% 0.0:0.1:3.0

% % % Previos Lower;
% % for i1=length(histArray_sub1(:,1))-1:-1:2
% % if histArray_sub1(i1,6)<=gradientLowerThreshould(NextT)
% % %     reporting the Temperature bin value
% % lowerBndry=histArray_sub1(i1-1,1);
% % break
% % end
% % end

% Modified the preivius method//this is good than previous
% stop two steps down before the mode top point
% % for i1=length(histArray_sub1(:,1))-2:-1:2
% % if histArray_sub1(i1,6)<=gradientLowerThreshould(NextT)
% % %     reporting the Temperature bin value
% % lowerBndry=histArray_sub1(i1,1);
% % break
% % end
% % end

% Try: from end of tail to top way
for i1=1:length(histArray_sub1(:,1))-2
if histArray_sub1(i1,6)>=gradientLowerThreshould(NextT)
% %     reporting the Temperature bin value
lowerBndry=histArray_sub1(i1,1);
break
end
end

% % 1.bin,2.Pxlcount,3.Re-scaleY(intensity %),4.PositionY,5.Posi.X,
% % 6.IntenctiyPernctDiff


%if the angle condition not existing  take the lowest temp
if lowerBndry==0
lowerBndry=histArray_sub1(1,1);

end

% also if the gradient position has exceeded lowerBreak repoint it to
% lowerBreak is the (vlaue that out of the order-sudden rise)
% make equal to the next highest before lowerbreak

if lowerBndry<lowerBreak
lowerBndry=lowerBreak;

end


%Upper

% upper threshold gradient % is higher
% array of gradient/s threshold/s
% % % % % % % % % % % % % % % % % % % % Check
gradientUpperThreshould=0.5;
% 0.0:0.1:5.0
% % % % % % % % % % % % % % % % % % % % 
% 0.3:0.1:0.7;
% 0.3:1.5:15.0
% 0.00:0.3:15.0
% 0.00:0.1:5.0
% 0.0:0.1:3.0

% Previous Upper
% for i2=2:length(histArray_sub2(:,1))
% if  histArray_sub2(i2,6)<=gradientUpperThreshould(NextT)   
% % record the start of bin temp which is the bin value of previous one
% upperBndry=histArray_sub2(i2,1);
% 
% break
% end
% end
% Modified Prievous
% % for i2=2:length(histArray_sub2(:,1))
% % if  histArray_sub2(i2,6)<=gradientUpperThreshould(NextT)   
% % % record the start of bin temp which is the bin value of previous one
% % upperBndry=histArray_sub2(i2,1);
% % 
% % break
% % end
% % end

% New try: from tail to top way
for i2=length(histArray_sub2(:,1)):-1:2
if  histArray_sub2(i2,6)>=gradientUpperThreshould(NextT)   
% record the start of bin temp which is the bin value of previous one
upperBndry=histArray_sub2(i2,1);

break
end
end

% %if the gradient condition not existing take the highst temp
if upperBndry==0
  %New:    
% upperBndry=histArray_sub2(end-1,1);

% old: 
upperBndry=histArray_sub2(end,1);
end


%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
%calculations 
%mean temperautre
averageArray=0;
for x=1:length(matrix(1,:))
for y=1:length(matrix(:,1))
    if  matrix(y,x) >= lowerBndry   &&  matrix(y,x) < upperBndry
   
        averageArray(length(averageArray(:,1))+1,1)=matrix(y,x);
    
    end
end
end
averageArray=averageArray(2:end,1);

% % % % % % % % % % % % % % % % % % % % % %Check 
% if comapring more than single only enable mean
% lower and upper is posible when one threshold is tested
results{pin1+1,4+NextT}=lowerBndry;
results{pin1+1,5+NextT}=upperBndry;
results{pin1+1,6+NextT}=mean(averageArray(:,1));
% % % % % % % % % % % % % % % % % % % % % % 

end %end of looping thresholds


disp(imgNum)


end
% >>>>>>>>>>>>>>>>>Calculating RMSE 
% should have the total images and their ReffMeans

% % % % converting cell to double fro 5th col onwards
% % % % % % % % % % % % % % % % % % % % % % Check
% % % subset=results(2:end,7:end);
% % % % % % % % % % % % % % % % % % % % % % 
% % % 
% % % subset=cell2table(subset);
% % % gradientMean=table2array(subset);
% % % rmseOut=0;
% % % rCoeffOrdinary=0;
% % % rCoeffAdj=0;
% % % for i=1:length(gradientMean(1,:)) %the # of cols
% % % 
% % % % % % % % % % % % % % % % % % % % % % % % % CHECK 
% % %     mdl=fitlm(gradientMean(:,i),reffTempSun);
% % % % % % % % % % % % % % % % % % % % % % % % % 
% % % 
% % % rmseOut(1,i)=mdl.RMSE;
% % % rCoeffOrdinary(1,i)=mdl.Rsquared.Ordinary;
% % % rCoeffAdj(1,i)=mdl.Rsquared.Adjusted;
% % % 
% % % end


% >>>>>>>>>>>>>>>>>>>>>>>>>>>>

xlswrite(strcat(imagePath,'Results.xlsx'),results);
disp('end');
endT=fix(clock);
process=endT-startT;

